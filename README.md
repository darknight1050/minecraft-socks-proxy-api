# Minecraft Socks Proxy API

Simple API to play minecraft through a socks proxy.
If you use this API, you will have to credit me (darknight1050).

**Add**
```java
ProxyManager.INSTANCE.setServer(ip, port);
if (ProxyManager.INSTANCE.hasProxy()) {
	if(!ProxyManager.INSTANCE.isProxyValid()){
		GuiConnecting.logger.error("Couldn\'t connect to proxy");
		GuiConnecting.this.mc.displayGuiScreen(new GuiDisconnected(GuiConnecting.this.previousGuiScreen, "connect.failed", new ChatComponentTranslation("disconnect.genericReason", new Object[] { "Invalid Proxy" })));
		return;
	}
	ProxyManager.INSTANCE.startProxy();
}
String proxyIp = ProxyManager.INSTANCE.getConnectIp();
int proxyPort = ProxyManager.INSTANCE.getConnectPort();
					
inetaddress = InetAddress.getByName(proxyIp);
GuiConnecting.this.networkManager = NetworkManager.func_181124_a(inetaddress, proxyPort, GuiConnecting.this.mc.gameSettings.func_181148_f());
GuiConnecting.this.networkManager.setNetHandler(new NetHandlerLoginClient(GuiConnecting.this.networkManager, GuiConnecting.this.mc, GuiConnecting.this.previousGuiScreen));
GuiConnecting.this.networkManager.sendPacket(new C00Handshake(47, ip, port, EnumConnectionState.LOGIN));
GuiConnecting.this.networkManager.sendPacket(new C00PacketLoginStart(GuiConnecting.this.mc.getSession().getProfile()));
```
**to GuiConnecting in net.minecraft.client.multiplayer**

![](./toAdd.png) 