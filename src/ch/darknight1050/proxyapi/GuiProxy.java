package ch.darknight1050.proxyapi;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import ch.darknight1050.client.Spectral;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;

public class GuiProxy extends GuiScreen {

	/** Text field containing the command block's command. */
	private GuiTextField address;

	/** "Done" button for the GUI. */
	private GuiButton setBtn;
	private GuiScreen lastScreen;

	public GuiProxy(GuiScreen lastScreen) {
		this.lastScreen = lastScreen;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen() {
		this.address.updateCursorCounter();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		this.buttonList.clear();
		this.buttonList.add(this.setBtn = new GuiButton(0, this.width / 2 - 4 - 150, this.height / 2 + 20, 150, 20, "Set Proxy"));
		this.buttonList.add(new GuiButton(1, this.width / 2 + 4, this.height / 2 + 20, 150, 20, I18n.format("gui.cancel", new Object[0])));
		this.buttonList.add(new GuiButton(2, this.width / 2 - 4 - 150, this.height / 2 - 10, 150, 20, "Clip Board"));
		this.buttonList.add(new GuiButton(3, this.width / 2 + 4, this.height / 2 - 10, 150, 20, "Clear Current"));
		this.address = new GuiTextField(2, this.fontRendererObj, this.width / 2 - 150, this.height / 2 - 40, 300, 20);
		this.address.setMaxStringLength("255.255.255.255:65535".length());
		this.address.setFocused(true);
		this.setBtn.enabled = this.address.getText().trim().length() > 0;
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.enabled) {
			if (button.id == 1) {
				this.mc.displayGuiScreen(this.lastScreen);
			} else if (button.id == 0) {
				if (this.address.getText().trim().length() > 0) {
					String[] splitted = this.address.getText().split(":");
					if (splitted.length == 2) {
						try {
							ProxyManager.INSTANCE.setProxy(splitted[0], Integer.valueOf(splitted[1]));
						} catch (Exception e) {
						}
						this.mc.displayGuiScreen(this.lastScreen);
					}
				}
			} else if (button.id == 2) {
				String[] splitted = getClipboardString().split(":");
				if (splitted.length == 2) {
					try {
						ProxyManager.INSTANCE.setProxy(splitted[0], Integer.valueOf(splitted[1]));
					} catch (Exception e) {
					}
					this.mc.displayGuiScreen(this.lastScreen);
				}
			} else if (button.id == 3) {
				ProxyManager.INSTANCE.clearProxy();
				this.mc.displayGuiScreen(this.lastScreen);
			}
		}
	}

	/**
	 * Fired when a key is typed (except F11 who toggle full screen). This is
	 * the equivalent of KeyListener.keyTyped(KeyEvent e). Args : character
	 * (character on the key), keyCode (lwjgl Keyboard key code)
	 */
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		this.address.textboxKeyTyped(typedChar, keyCode);
		this.setBtn.enabled = this.address.getText().trim().length() > 0;
	}

	/**
	 * Called when the mouse is clicked. Args : mouseX, mouseY, clickedButton
	 */
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.address.mouseClicked(mouseX, mouseY, mouseButton);
	}

	/**
	 * Draws the screen and all the components in it. Args : mouseX, mouseY,
	 * renderPartialTicks
	 */
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		this.drawString(this.fontRendererObj, "Proxy:", this.width / 2 - 12, 20, 16777215);
		this.drawString(this.fontRendererObj, "Address:", this.width / 2 - 150, this.height / 2 - 60, 10526880);
		this.address.drawTextBox();

		super.drawScreen(mouseX, mouseY, partialTicks);
	}

}
