package ch.darknight1050.proxyapi;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicInteger;

public class ProxyThread extends Thread {

	private static final AtomicInteger THREAD_ID = new AtomicInteger(0);

	private boolean stopped;
	private ProxyManager proxyManager;
	private String serverIP;
	private int serverPort;

	public ProxyThread(ProxyManager proxyManager, String serverIP, int serverPort) {
		super("Proxy #" + THREAD_ID.incrementAndGet());
		stopped = false;
		this.proxyManager = proxyManager;
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}

	@Override
	public void run() {
		ServerSocket serverSocket = null;
		Socket client = null;
		Socket server = null;
		try {
			serverSocket = new ServerSocket(0);
			this.proxyManager.setLocalPort(serverSocket.getLocalPort());
			server = serverSocket.accept();
			InputStream serverIn = server.getInputStream();
			OutputStream serverOut = server.getOutputStream();
			client = new Socket(this.proxyManager.getProxy());
			client.connect(new InetSocketAddress(this.serverIP, this.serverPort), ProxyManager.TIMEOUT);
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			byte[] bytes;
			int available;
			while (!this.stopped) {
				if ((available = clientIn.available()) > 0) {
					bytes = new byte[available];
					clientIn.read(bytes);
					serverOut.write(bytes);
					serverOut.flush();
				}
				if ((available = serverIn.available()) > 0) {
					bytes = new byte[available];
					serverIn.read(bytes);
					clientOut.write(bytes);
					clientOut.flush();
				}
				Thread.sleep(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (serverSocket != null && !serverSocket.isClosed())
					serverSocket.close();
				if (client != null && !client.isClosed())
					client.close();
				if (server != null && !server.isClosed())
					server.close();
			} catch (Exception e) {
			}
		}
	}

	public void stopProxy() {
		this.stopped = true;
	}
}
