package ch.darknight1050.proxyapi;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;

public class ProxyManager {

	public static ProxyManager INSTANCE;

	public static final int TIMEOUT = 4000;

	private Proxy proxy = null;
	private String serverIp;
	private int serverPort;
	private ProxyThread thread = null;
	private int localPort;

	static {
		INSTANCE = new ProxyManager();
	}

	private ProxyManager() {
	}

	public void clearProxy() {
		this.proxy = null;
	}

	public void setProxy(String proxyIp, int proxyPort) {
		this.proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(proxyIp, proxyPort));
	}

	public void setServer(String serverIp, int serverPort) {
		this.serverIp = serverIp;
		this.serverPort = serverPort;
	}

	public Proxy getProxy() {
		return this.proxy;
	}

	public int getLocalPort() {
		return this.localPort;
	}

	public void setLocalPort(int localPort) {
		this.localPort = localPort;
	}

	public String getConnectIp() {
		if (hasProxy())
			return "127.0.0.1";
		return this.serverIp;
	}

	public int getConnectPort() {
		if (hasProxy())
			return this.getLocalPort();
		return this.serverPort;
	}

	public boolean hasProxy() {
		return this.proxy != null && !(this.serverIp.equalsIgnoreCase("127.0.0.1") || this.serverIp.equalsIgnoreCase("localhost"));
	}

	public boolean isProxyValid() {
		if (!this.hasProxy())
			return false;
		try {
			Socket socket = new Socket(proxy);
			socket.connect(InetSocketAddress.createUnresolved("google.com", 80), TIMEOUT);
			socket.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void startProxy() {
		if (!this.hasProxy())
			return;
		if (this.thread != null) 
			this.thread.stopProxy();
		this.thread = new ProxyThread(this, this.serverIp, this.serverPort);
		this.thread.start();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
